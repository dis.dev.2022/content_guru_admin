export default () => {
    document.addEventListener('click', tree);

    function tree(event) {

        if (event.target.closest('[data-tree-switcher]')) {
            const treeNode = event.target.closest('[data-tree-node]');
            treeNode.classList.toggle('open');
        }
        else if (event.target.closest('[data-tree-item]')) {
            const treeItem = event.target.closest('[data-tree-item]');
            treeItem.classList.toggle('active');
        }
    }

};
