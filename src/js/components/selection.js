export default () => {

    document.addEventListener('click', selectionToggle);

    function selectionToggle(event) {
        const selections = document.querySelectorAll('[data-selection]');

        if (selections.length > 0) {
            if (event.target.closest('[data-selection-toggle]')) {
                let selectionContainer = event.target.closest('[data-selection]');
                if (selectionContainer.classList.contains('open')) {
                    selectionContainer.classList.remove('open');
                }
                else {
                    selectionClose();
                    selectionContainer.classList.add('open');
                }
            }
            else if(event.target.closest('[data-selection-item]')) {
                let item = event.target.closest('[data-selection-item]');
                let itemContainer = event.target.closest('[data-selection]');
                let itemInput = itemContainer.querySelector('[data-selection-input]');
                let itemActive = itemContainer.querySelector('[data-selection-chosen]');

                itemContainer.classList.remove('open');
                itemInput.value = item.dataset.selectionItem;
                itemActive.innerHTML = item.innerHTML;
            }

            else {
                selectionClose();
            }
            function selectionClose() {
                selections.forEach(el => {
                    el.classList.remove('open');
                });
            }
        }
    }
};
