// Подключение из node_modules
import * as noUiSlider from 'nouislider';
import * as usefulFunctions from "../components/functions.js";

export function rangeInit() {

    const dataSliders = document.querySelectorAll('[data-range-slider]');
    if (dataSliders.length > 0) {
        dataSliders.forEach(el => {
            let slider = el;
            let sliderStart = el.getAttribute('data-start').split(",").map(parseFloat);
            let sliderMin = parseFloat(el.getAttribute('data-min'));
            let sliderMax = parseFloat(el.getAttribute('data-max'));
            console.log(el)
            noUiSlider.create(slider, {
                start: sliderStart,
                connect: 'lower',
                range: {
                    'min': sliderMin,
                    'max': sliderMax,
                }
            });
            slider.noUiSlider.on('update', function (values, handle) {
                slider.closest('[data-range]').querySelector('[data-range-value]').innerHTML = usefulFunctions.getDigFormat(parseInt(values[handle]))
            });
        });
    }
}
rangeInit();
