"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import SimpleBar from 'simplebar'; // Кастомный скролл
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import Selection from "./components/selection.js";
import TreeView from "./components/treeview.js";
import {WebpMachine} from "webp-hero";


// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Selection
Selection();

// Tree view
TreeView();

// Custom Scroll
if (document.querySelectorAll('[data-simplebar]').length) {
    document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
        new SimpleBar(scrollBlock, {
            autoHide: false,
        });
    });
}

document.addEventListener('click', Edit);

function Edit(event) {
    const editCollection = document.querySelectorAll('[data-edit]');
    if (event.target.closest('[data-edit-button]')) {
        EditClose();
        const container =  event.target.closest('[data-edit]');
        container.classList.add('edit-open');
    }
    else if (event.target.closest('[data-edit-save]'))  {
        EditClose();
        const container =  event.target.closest('[data-edit]');
        const containerText = container.querySelector('[data-edit-content]')
        const containerValue = container.querySelector('[data-edit-field]').value;
        containerText.innerHTML = containerValue;

        container.classList.remove('edit-open');
    }
    else if (event.target.closest('[data-edit]')) {

    }
    else {
        EditClose();
    }

    function EditClose() {
        editCollection.forEach(el => {
            el.classList.remove('edit-open');
        });
    }
}

document.addEventListener('click', EditTags);

function EditTags(event) {
    if (event.target.closest('[data-tags-edit]')) {
        const container =  event.target.closest('[data-tags]');
        container.classList.toggle('tags--edit');
    }
    else if(event.target.closest('[data-tag-remove]')) {
        event.target.closest('[data-tag]').remove();
    }
    else if(event.target.closest('[data-tags-new]')) {
        document.querySelector('[data-tags-form]').classList.add('open');
    }
    else if(event.target.closest('[data-tags-close]')) {
        document.querySelector('[data-tags-form]').classList.remove('open');
    }
    else if(event.target.closest('[data-tags-submit]')) {
        let tagText = event.target.closest('[data-tags-form]').querySelector('[data-tags-input]').value;
        let tagItem = document.createElement('div');
        let tagItemText = document.createElement('span');
        let tagItemRemove= document.createElement('i');

        document.querySelector('[data-tags-form]').classList.remove('open');

        tagItem.classList.add('tag');
        tagItem.textContent = tagText;
        tagItem.setAttribute('data-tag', '');

        tagItemRemove.setAttribute('data-tag-remove', '');
        tagItem.appendChild(tagItemRemove);

        document.querySelector('[data-tags-main]').appendChild(tagItem);
    }
    else {

    }
}

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false,
    type: "inline",
});

Fancybox.defaults.ScrollLock = false;

$('[data-dates]').daterangepicker();

document.addEventListener('click', Loading);

function Loading(event) {
    if (event.target.closest('[data-loaded-open]')) {
        document.querySelector('body').classList.add('data-open') ;
    }
}

document.addEventListener('click', tableSecond);

function tableSecond(event) {

    if (event.target.closest('[data-table-switcher]')) {
        const treeGroup = event.target.closest('tbody');
        treeGroup.classList.toggle('open');
    }
}

document.addEventListener('click', tooltipClose);

function tooltipClose(event) {

    if (event.target.closest('[data-tooltip-close]')) {
        const treeGroup = event.target.closest('[data-tooltip]');
        treeGroup.style.display = 'none';
    }
}

window.addEventListener("load", function (e) {

    if(document.querySelector('#editor')) {

        setTimeout( () =>
        {
            var quill = new Quill('#editor', {
                theme: 'snow',
                modules: {
                    toolbar: [
                        ['bold', 'italic', 'underline', 'strike'],
                        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                        [{ 'indent': '-1'}, { 'indent': '+1' }],
                        [{ 'size': ['small', false, 'large', 'huge'] }],
                        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                        [{ 'color': [] }, { 'background': [] }],
                        [{ 'align': [] }],
                        ['clean']
                    ]
                },
            });
        }, 300 );
    }
});


import "./forms/range.js";
